# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('karate', '0010_videos'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='videos',
            options={'verbose_name': '\u0412\u0438\u0434\u0435\u043e', 'verbose_name_plural': '\u0412\u0438\u0434\u0435\u043e'},
        ),
        migrations.AlterField(
            model_name='videos',
            name='href',
            field=models.CharField(max_length=255, verbose_name='\u0423\u0440\u043b'),
        ),
    ]
